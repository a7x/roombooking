<?php include_once("partials/meta_header.php"); ?>
<?php include_once("partials/main_header.php"); ?>

<div class="content js-content">
    <div class="yja">
        <div class="yja-12">
            <div class="big-margin-leader big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">
                        Hasil Pencarian
                    </div>
                </div>
                <?php for($i =1; $i < 3; $i++):?>
                <div class="panel">
                    <div class="panel__body margin-trailer">
                        <div class="panel__left">
                            <div class="thumb">
                                <div class="thumb__body thumb__body--card-list">
                                    <div class="thumb__img" style="background-image: url(http://sanyrosahotel.com/img/meeting.png);"></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel__middle">
                            <div class="heading">
                                <div class="heading__text heading__text--medium small-margin-trailer border-bottom">
                                    Blue Room
                                    <span class="heading__sub">Ampera 22</span>
                                </div>
                            </div>
                            <div class="yja">
                                <div class="yja-3">
                                    <div class="badge">
                                        <a href="#" class="badge__link">
                                            <span class="badge__text">16.00 - 17.00</span>
                                        </a>
                                    </div>
                                    <div class="badge">
                                        <a href="#" class="badge__link">
                                            <span class="badge__text">15.00 - 16.00</span>
                                        </a>
                                    </div>
                                    <div class="badge">
                                        <a href="#" class="badge__link is-disable">
                                            <span class="badge__text">11.00 - 13.00</span>
                                        </a>
                                    </div>
                                    <a href="#" class="font-small text-uppercase modal-trigger" data-modal="modal-time">View More</a>
                                </div>
                                <div class="yja-9">
                                    <div class="article">
                                        <h3>Fasilitas:</h3>
                                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consequatur rerum accusamus in neque ullam omnis iure at rem sit impedit laudantium cumque debitis aperiam perspiciatis, odit quas delectus eaque distinctio.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel__right">
                            <div class="panel__button">
                                <div class="button">
                                    <a href="booking.php" class="button__body button__body--strong">Book Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</div>

<?php include_once("partials/main_footer.php"); ?>
<?php include_once("partials/meta_footer.php"); ?>
