$(document).ready(function() {
    // var HOST = "http://localhost:8080";
    var HOST = "http://80eb8380.ngrok.io/";
    var rooms = {};

    getBuildings();

    function getBuildings() {
        var bid = getParameterByName("bid");
        $.ajax({
            url: HOST + "/buildings",
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            success: function(data) {
                updateSelectOptions('#buildings_opts', data);
                updateSelectOptions('#form_building', data);
                if(bid) $("#buildings_opts").val(bid);
                $('select option[value="'+bid+'"]').attr("selected",true);
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    function updateSelectOptions(name, data) {
        var $el = $(name);
        $el.empty();
        $.each(data.data, function(key, value) {
            $el.append($("<option></option>").attr("value", value.id).text(value.name));
        });
    }

    function addSearchResult(data) {
        var panel = '<div class="panel">'
        panel += '<div class="panel__body margin-trailer">'
        panel += '<div class="panel__left">'
        panel += '<div class="thumb">'
        panel += '<div class="thumb__body thumb__body--card-list">'
        panel += '<div class="thumb__img" style="background-image: url(http://sanyrosahotel.com/img/meeting.png);"></div>'
        panel += '</div></div></div>'
        panel += '<div class="panel__middle">'
        panel += '<div class="heading">'
        panel += '<div class="heading__text heading__text--medium small-margin-trailer border-bottom">'
        panel += data.room_name
        panel += '<span class="heading__sub">' + data.building_name + '</span>'
        panel += '</div></div>'
        panel += '<div class="yja"><div class="yja-3">'
        panel += '<a href="#" class="font-small text-uppercase modal-trigger" data-modal="modal-time" data-id="' + data.room_id + '">View More</a>'
        panel += '</div>'
        panel += '<div class="yja-9"><div class="article"><h3>Fasilitas:</h3>'
        panel += '<p>' + data.facilities + '</p>'
        panel += '</div></div></div></div>'
        panel += '<div class="panel__right"><div class="panel__button"><div class="button">'
        panel += '<a href="booking.html?id='+data.room_id+'&bid='+data.building_id+'" class="button__body button__body--strong">Book Now</a>'
        panel += '</div></div></div></div></div>'

        $("#results").append($.parseHTML(panel));
    }

    function bindRoomId() {
        $('.modal-trigger').click(function(e) {
            e.preventDefault();

            var roomId = $(this).data('id');
            var data = rooms[roomId];
            var timetable = new Timetable();
            timetable.addLocations([data.room_name]);
            for (var i = 0; i < data.meetings.length; i++) {
                if (data.meetings[i].booked_start_date) {
                    var start = new Date(data.meetings[i].booked_start_date);
                    var end = new Date(data.meetings[i].booked_end_date);
                    timetable.addEvent(data.meetings[i].meeting_name, data.room_name, start, end);
                }
            }
            var renderer = new Timetable.Renderer(timetable);
            renderer.draw('.timetable');
        });
    }

    function findAvailableRooms() {
        var date = $("#date").val();
        var time = $("#time").val();
        var buildingId = $("#buildings_opts").val();


        if (!date || !time || !buildingId) return;

        var dateTime = new Date(date + " " + time);
        var m = ("0" + (dateTime.getMonth() + 1)).slice(-2);
        var d = ("0" + dateTime.getDate()).slice(-2);
        var dateString = dateTime.getFullYear() + "-" + m + "-" + d;

        $.ajax({
            url: HOST + "/rooms/available/" + buildingId + "/" + dateString,
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            success: function(data) {
                $("#results").empty();
                $.each(data.data, function(key, value) {
                    rooms[value.room_id] = value;
                    addSearchResult(value);
                });
                checkModalInitialStatus();
                bindModalTrigger();
                bindRoomId();
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    getAllRooms();

    function getRooms(buildingId) {
        $.ajax({
            url: HOST + "/buildings/ " + buildingId + "/rooms",
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            success: function(data) {
                var $el = $('#form_room');
                $el.empty();
                $.each(data.data, function(key, value) {
                    $el.append($("<option></option>").attr("value", value.id).text(value.room_name));
                });
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    function getAllRooms() {
        var id = getParameterByName("id");
        $.ajax({
            url: HOST + "/rooms",
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            success: function(data) {
                var $el = $('#form_room');
                $el.empty();
                $.each(data.data, function(key, value) {
                    $el.append($("<option></option>").attr("value", value.id).text(value.room_name));
                });
                $('select option[value="'+id+'"]').attr("selected",true);
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    function buildDate(dateTime) {
        var hour = (dateTime.getHours() < 10 ? '0' : '') + dateTime.getHours();
        var min = (dateTime.getMinutes() < 10 ? '0' : '') + dateTime.getMinutes();
        var time = hour + ":" + min;
        var m = ("0" + (dateTime.getMonth() + 1)).slice(-2);
        var d = ("0" + dateTime.getDate()).slice(-2);
        var dateString = dateTime.getFullYear() + "-" + m + "-" + d + " " + time;
        return dateString;
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function addMyBooking(meeting, room) {
        var start = new Date(meeting.booked_start_date);
        var end = new Date(meeting.booked_end_date);
        var m = ("0" + (start.getMonth() + 1)).slice(-2);
        var d = ("0" + start.getDate()).slice(-2);
        var shour = (start.getHours() < 10 ? '0' : '') + start.getHours();
        var smin = (start.getMinutes() < 10 ? '0' : '') + start.getMinutes();
        var ehour = (end.getHours() < 10 ? '0' : '') + end.getHours();
        var emin = (end.getMinutes() < 10 ? '0' : '') + end.getMinutes();


        var box = '<div class="box"><div class="box__body"><div class="blocklist blocklist--insidebox">';
        box += '<div class="blocklist__item"><div class="blocklist__link blocklist__link--nohover"><div class="blocklist__text"><div class="heading">';
        box += '<div class="heading__text heading__text--small small-margin-trailer border-bottom">';
        box += room.room_name;
        box += '<span class="heading__sub">' + room.building_name + '</span>';
        box += '</div></div>';
        box += '<div class="yja no-padding"><div class="yja-4"><div class="article"><h3>Waktu booking:</h3></div>';
        box += '<div class="badge"><div class="badge__link">';
        box += '<span class="badge__text">' + d + " - " + m + " - " + start.getFullYear() + '</span>';
        box += '</div></div>';
        box += '<div class="badge inline-block"><div class="badge__link">';
        box += '<span class="badge__text">' + shour + '.' + smin + '</span>';
        box += '</div></div>-';
        box += '<div class="badge inline-block"><div class="badge__link">';
        box += '<span class="badge__text">' + ehour + '.' + emin + '</span>';
        box += '</div></div></div>';
        box += '<div class="yja-8"><div class="article"><h3>Fasilitas:</h3>';
        box += '<p>' + room.facilities + '</p>';
        box += '</div></div></div></div></div></div></div></div></div>';

        $("#my_bookings").append($.parseHTML(box));
    }

    loadMyBookings();

    function loadMyBookings() {
        var email = localStorage.getItem('emailBR');
        $.ajax({
            url: HOST + "/rooms/list/" + email,
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            success: function(data) {
                $("#my_bookings").empty();
                $.each(data.data, function(key, value) {
                    if (!value.meetings) return;
                    $.each(value.meetings, function(key1, value2) {
                        addMyBooking(value2, value);
                    });
                });
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    setLoggedInUser();

    function setLoggedInUser() {
        var email = localStorage.getItem('emailBR');
        $('#form_user_name').val(email);
    }

    $('#find_room').click(function(e) {
        e.preventDefault();
        findAvailableRooms();
    });

    $('#form_building').on('change', function() {
        var buildingId = $('#form_building').val();
        getRooms(buildingId);
    });

    $('#form_submit').click(function(e) {
        e.preventDefault();

        var buildingId = $('#form_building').val();
        var roomId = $('#form_room').val();
        var buildingName = $('#form_building option:selected').text();
        var roomName = $('#form_room option:selected').text();
        var name = $('#form_user_name').val();
        var meeting_name = $('#form_meeting_name').val();
        var date = $('#form_date').val();
        var start = $('#form_start').val();
        var end = $('#form_end').val();
        var startTime = new Date(date + " " + start);
        var endTime = new Date(date + " " + end);

        var payload = {
            "building_id": buildingId,
            "room_id": roomId,
            "building_name": buildingName,
            "room_name": roomName,
            "booked_start_date": buildDate(startTime),
            "booked_end_date": buildDate(endTime),
            "booked_by": name,
            "meeting_name": meeting_name
        }
        var json_data = JSON.stringify(payload);

        $.ajax({
            url: HOST + "/rooms/book",
            type: "POST",
            data: json_data,
            dataType: "json",
            crossDomain: true,
            contentType: 'application/json',
            success: function(data) {
                openModal('modal-success-save');
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });
});
