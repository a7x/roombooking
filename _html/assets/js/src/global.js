// MODAL ====================================

function openModal(id) {
    $('#'+id).fadeIn('fast');
    $('body').addClass('noScroll');
    $('.wrapper').addClass('js-blur');
    $('.footer').addClass('js-blur');
}

function closeModal() {
    $('.modal').fadeOut('fast');
    $('body').removeClass('noScroll');
    $('.wrapper').removeClass('js-blur');
    $('.footer').removeClass('js-blur');
}

function bindModalTrigger() {
    $('.modal-trigger').click(function(e)
    {
        var target = $(this).data('modal');

        closeModal();
        openModal(target);
        e.preventDefault();

    });

    $('.modal').click(function() {
        closeModal();
    }).children().click(function(e) {
        e.stopPropagation();
    });

    $('.modal-close-trigger').click(function(e) {
        closeModal();
        e.preventDefault();
    });
}


function checkModalInitialStatus() {
    if($('.modal').hasClass('active'))
    {
        $('body').addClass('noScroll');
    }
}

// STICKY FOOTER ====================================

function stickyFooter() {
    var headerHeight = $('.js-header').outerHeight();
    var footerHeight = $('.js-footer').outerHeight();
    $('.js-content').css({
        'padding-top': headerHeight
    });

    $('.js-footer').css({
        'margin-top': -footerHeight
    });
}

$(document).ready(function() {

    // sticky footer
    stickyFooter();

    // modal
    checkModalInitialStatus();
    bindModalTrigger();

    $(".js-datepicker").pickadate({
        monthsFull: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
        weekdaysFull: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
        weekdaysShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
        today: 'Hari Ini',
        clear: 'Hapus',
        close: 'Batalkan',
        selectYears: true,
        format: 'dd mmmm yyyy',
    });

    $(".js-timepicker").pickatime({
        clear: 'Hapus',
    });

});
