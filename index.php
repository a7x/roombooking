<?php include_once("partials/meta_header.php"); ?>
<?php include_once("partials/main_header.php"); ?>

<div class="content js-content">
    <div class="yja">
        <div class="yja-12">
            <div class="big-margin-leader big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">
                        Login Dengan Akun Bukalapak
                    </div>
                </div>
                <div class="box">
                    <div class="box__body big-padding-leader big-padding-trailer">
                        <div class="text-center">
                            <div class="heading">
                                <div class="heading__text margin-trailer weight-normal">
                                    Klik tombol di bawah ini untuk login dengan akun Bukalapak:
                                </div>
                            </div>
                            <div class="button">
                                <a href="#" class="button__body button__body--strong">Login With Bukalapak</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once("partials/main_footer.php"); ?>
<?php include_once("partials/meta_footer.php"); ?>
