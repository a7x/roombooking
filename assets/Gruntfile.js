module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    compass: {
      dist: {
        options: {
          config: 'config/compass.rb'
        }
      }
    },

    browserSync: {
        bsFiles: {
          src : [
            'css/*.css',
            'scss/**/*.scss',
            '**/*.php',
            '../*.php',
            '../partials/*.php',
            '**/*.html',
            'js/*.js'
          ]
        },
        options: {
          proxy: "local.host:8888/roombooking",
          watchTask: true,
          port: 8081
        }
    },

    concat: {
        js: {
          src: [
            'js/vendor/jquery/dist/jquery.min.js',
            'js/vendor/pickadate/lib/picker.js',
            'js/vendor/pickadate/lib/legacy.js',
            'js/vendor/pickadate/lib/picker.date.js',
            'js/vendor/pickadate/lib/picker.time.js',
            'js/src/global.js'
          ],
          dest: 'js/all.js'
        }
    },

    uglify: {
      js: {
        files: {
            'js/all.min.js' : [ 'js/all.js' ]
        }
      }
    },

    watch: {
      markup: {
        files: [
          '*.php',
          '**/*.php',
          'partials/*.php'
        ]
      },
      css: {
        files: "scss/**/*.scss",
        tasks: ['compass']
      },
      concat: {
        files: [
          'js/vendor/jquery/dist/jquery.min.js',
          'js/vendor/pickadate/lib/picker.js',
          'js/vendor/pickadate/lib/legacy.js',
          'js/vendor/pickadate/lib/picker.date.js',
          'js/vendor/pickadate/lib/picker.time.js',
          'js/src/global.js'
        ],
        tasks: ['concat']
      },
      uglify: {
        files: "js/all.js",
        tasks: ['uglify']
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Default task(s).
  grunt.registerTask('default', ['browserSync','watch']);

};
