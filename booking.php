<?php include_once("partials/meta_header.php"); ?>
<?php include_once("partials/main_header.php"); ?>

<div class="content js-content">
    <div class="yja">
        <div class="yja-12">
            <div class="big-margin-leader big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">
                        Booking Ruangan
                    </div>
                </div>
                <form action="#" class="input">
                    <div class="input__item input__item--with-icon">
                        <input type="text" class="input__field" placeholder="Isi Nama Anda">
                        <div class="input__icon">
                            <i class="icon icon-pencil"></i>
                        </div>
                    </div>
                    <div class="input__item input__item--with-icon">
                        <input type="text" class="input__field" placeholder="Isi Nama Meeting">
                        <div class="input__icon">
                            <i class="icon icon-pencil"></i>
                        </div>
                    </div>
                    <div class="yja no-padding">
                        <div class="yja-6" style="margin-right: 20px;">
                            <div class="input__item input__item--with-icon">
                                <input type="text" class="input__field js-datepicker" placeholder="Pilih Tanggal">
                                <div class="input__icon">
                                    <i class="icon icon-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="yja-6">
                            <div class="input__item input__item--with-icon">
                                <input type="text" class="input__field js-timepicker" placeholder="Pilih Jam">
                                <div class="input__icon">
                                    <i class="icon icon-world"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input__item input__item--with-icon">
                        <select name="#" id="#" class="input__field">
                            <option value="" selected="selected">Pilih Gedung</option>
                            <option value="Value 1">Jason Ampera 22</option>
                            <option value="Value 1">Plaza City View</option>
                        </select>
                        <div class="input__icon">
                            <i class="icon icon-chevron-down-2"></i>
                        </div>
                    </div>
                    <div class="input__item input__item--with-icon">
                        <select name="#" id="#" class="input__field">
                            <option value="" selected="selected">Pilih Ruangan</option>
                            <option value="Value 1">Jason Ampera 22</option>
                            <option value="Value 1">Plaza City View</option>
                        </select>
                        <div class="input__icon">
                            <i class="icon icon-chevron-down-2"></i>
                        </div>
                    </div>
                    <div class="text-center margin-leader">
                        <div class="button">
                            <div class="button__body button__body--strong modal-trigger" data-modal="modal-success-save">Simpan Data</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include_once("partials/main_footer.php"); ?>
<?php include_once("partials/meta_footer.php"); ?>
