 </div> <!-- wrapper -->


<footer class="footer js-footer">
    <div class="yja">
        <div class="yja-12">
            &copy; 2017 BukaRuangan. All rights reserved
        </div>
    </div>
</footer>

<div id="modal-time" class="modal js-closemodal">
    <div class="modal__title">
        Availability
    </div>
    <div class="modal__body">
        <div class="modal__close modal-close-trigger color-white">
            <i class="icon icon-cancel"></i> Close
        </div>
        <div class="modal__content">
            <div class="text-center margin-trailer">
                <div class="yja">
                    <div class="yja-4 align-center">
                        <div class="badge">
                            <a href="#" class="badge__link">
                                <span class="badge__text">15.00 - 18.00</span>
                            </a>
                        </div>
                        <div class="badge">
                            <a href="#" class="badge__link">
                                <span class="badge__text">16.00 - 17.00</span>
                            </a>
                        </div>
                        <div class="badge">
                            <a href="#" class="badge__link is-disable">
                                <span class="badge__text">15.00 - 16.00</span>
                            </a>
                        </div>
                    </div>
                    <div class="yja-4 align-center">
                        <div class="badge">
                            <a href="#" class="badge__link is-disable">
                                <span class="badge__text">14.00 - 15.00</span>
                            </a>
                        </div>
                        <div class="badge">
                            <a href="#" class="badge__link is-disable">
                                <span class="badge__text">13.00 - 14.00</span>
                            </a>
                        </div>
                        <div class="badge">
                            <a href="#" class="badge__link is-disable">
                                <span class="badge__text">12.00 - 13.00</span>
                            </a>
                        </div>
                    </div>
                    <div class="yja-4 align-center">
                        <div class="badge">
                            <a href="#" class="badge__link is-disable">
                                <span class="badge__text">11.00 - 12.00</span>
                            </a>
                        </div>
                        <div class="badge">
                            <a href="#" class="badge__link is-disable">
                                <span class="badge__text">10.00 - 11.00</span>
                            </a>
                        </div>
                        <div class="badge">
                            <a href="#" class="badge__link is-disable">
                                <span class="badge__text">09.00 - 10.00</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="">
                <div class="button align-center">
                    <button class="button__body button__body--strong modal-close-trigger">
                        Book Now
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-success-save" class="modal js-closemodal">
    <div class="modal__title">
        Informasi
    </div>
    <div class="modal__body">
        <div class="modal__close modal-close-trigger color-white">
            <i class="icon icon-cancel"></i> Close
        </div>
        <div class="modal__content text-center">
            <div class="heading margin-trailer">
                <div class="heading__text heading__text--medium">
                    Data Anda berhasil disimpan
                </div>
            </div>
            <div class="text-center">
                <div class="button text-center">
                    <button class="button__body modal-close-trigger">
                        OK
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
