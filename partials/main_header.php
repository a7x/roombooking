	<header class="header js-header">
		<div class="yja">
            <div class="yja-1">
                <div class="logo">
                    <a href="index.php" class="logo__link"></a>
                </div>
            </div>
            <div class="yja-9">
                <div class="header__search pos-relative">
                    <form action="#">
                        <div class="input input--flex">
                            <div class="input__item input__item--with-icon no-margin">
                                <select name="#" id="#" class="input__field">
                                    <option value="" selected="selected">Pilih gedung</option>
                                    <option value="Value 1">Value 1</option>
                                    <option value="Value 1">Value 1</option>
                                </select>
                                <div class="input__icon">
                                    <i class="icon icon-search-find"></i>
                                </div>
                            </div>
                            <div class="input__item no-margin">
                                <input type="search" class="input__field js-datepicker" placeholder="Pilih tanggal">
                            </div>
                            <div class="input__item no-margin">
                                <input type="search" class="input__field js-timepicker" placeholder="Pilih jam">
                            </div>
                            <div class="input__item no-margin">
                                <div class="button">
                                    <button class="button__body button__body--for-header">Cari</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="yja-2 pos-relative">
                <div class="avatar avatar--with-dropdown avatar--for-header">
                    <div class="avatar__body">
                        MENU
                    </div>
                    <div class="avatar__icon">
                        <i class="icon icon-chevron-down-2"></i>
                    </div>
                    <div class="box box--for-dropdown">
                        <div class="box__body">
                            <div class="box__arrow"></div>
                            <div class="blocklist blocklist--insidebox">
                                <div class="blocklist__item">
                                    <a href="login.php" class="blocklist__link blocklist__link--with-icon-right">
                                        <span class="blocklist__icon">
                                            <i class="icon icon-right-open-big"></i>
                                        </span>
                                        <span class="blocklist__text">Login</span>
                                    </a>
                                    <a href="search-result.php" class="blocklist__link blocklist__link--with-icon-right">
                                        <span class="blocklist__icon">
                                            <i class="icon icon-right-open-big"></i>
                                        </span>
                                        <span class="blocklist__text">Search Result</span>
                                    </a>
                                    <a href="booking.php" class="blocklist__link blocklist__link--with-icon-right">
                                        <span class="blocklist__icon">
                                            <i class="icon icon-right-open-big"></i>
                                        </span>
                                        <span class="blocklist__text">Booking Ruangan</span>
                                    </a>
                                    <a href="my-booking.php" class="blocklist__link blocklist__link--with-icon-right">
                                        <span class="blocklist__icon">
                                            <i class="icon icon-right-open-big"></i>
                                        </span>
                                        <span class="blocklist__text">My Booking</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</header>
