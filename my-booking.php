<?php include_once("partials/meta_header.php"); ?>
<?php include_once("partials/main_header.php"); ?>

<div class="content js-content">
    <div class="yja">
        <div class="yja-12">
            <div class="big-margin-leader big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">
                        My Booking
                    </div>
                </div>
                <div class="box">
                <div class="box__body">
                    <div class="blocklist blocklist--insidebox">
                        <div class="blocklist__item">
                            <div class="blocklist__link blocklist__link--nohover">
                                <div class="blocklist__text">
                                    <div class="heading">
                                        <div class="heading__text heading__text--small small-margin-trailer border-bottom">
                                            Blue Room
                                            <span class="heading__sub">Ampera 22</span>
                                        </div>
                                    </div>
                                    <div class="yja no-padding">
                                        <div class="yja-4">
                                            <div class="article">
                                                <h3>Waktu booking:</h3>
                                            </div>
                                            <div class="badge">
                                                <div class="badge__link">
                                                    <span class="badge__text">17 November 2017</span>
                                                </div>
                                            </div>
                                            <div class="badge inline-block">
                                                <div class="badge__link">
                                                    <span class="badge__text">07.00</span>
                                                </div>
                                            </div>
                                            -
                                            <div class="badge inline-block">
                                                <div class="badge__link">
                                                    <span class="badge__text">08.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="yja-8">
                                            <div class="article">
                                                <h3>Fasilitas:</h3>
                                                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consequatur rerum accusamus in neque ullam omnis iure at rem sit.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

<?php include_once("partials/main_footer.php"); ?>
<?php include_once("partials/meta_footer.php"); ?>
