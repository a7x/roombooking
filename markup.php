<?php include_once("partials/meta_header.php"); ?>
<?php include_once("partials/main_header.php"); ?>

<div class="content js-content">
    <div class="yja">
        <div class="yja-12">
            <div class="big-margin-leader big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">Button & Modal</div>
                </div>
                <div class="button margin-trailer">
                    <div class="button__body modal-trigger" data-modal="modal-alert">View Modal</div>
                </div>
                <div class="button">
                    <div class="button__body button__body--strong">Book Now</div>
                </div>
            </div>
            <div class="big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">Avatar</div>
                </div>
                <div class="avatar">
                    <div class="avatar__head">
                        <img src="http://www.fillmurray.com/200/200" alt="" class="avatar__img">
                    </div>
                    <div class="avatar__body">
                        Fill Muray Lorem Dolor
                    </div>
                </div>
            </div>
            <div class="big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">Avatar with dropdown</div>
                </div>
                <div class="avatar avatar--with-dropdown">
                    <div class="avatar__head">
                        <img src="http://www.fillmurray.com/200/200" alt="" class="avatar__img">
                    </div>
                    <div class="avatar__body">
                        Fill Muray Lorem Dolor
                    </div>
                    <div class="avatar__icon">
                        <i class="icon icon-chevron-down-2"></i>
                    </div>
                </div>
            </div>
            <div class="big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">Vertical block list</div>
                </div>
                <div class="blocklist">
                    <div class="blocklist__item">
                        <a href="#" class="blocklist__link blocklist__link--with-icon-left">
                            <span class="blocklist__icon">
                                <i class="icon icon-globe"></i>
                            </span>
                            <span class="blocklist__text">List</span>
                        </a>
                    </div>
                    <div class="blocklist__item">
                        <a href="#" class="blocklist__link blocklist__link--with-icon-left">
                            <span class="blocklist__icon">
                                <i class="icon icon-globe"></i>
                            </span>
                            <span class="blocklist__text">List</span>
                        </a>
                    </div>
                    <div class="blocklist__item">
                        <a href="#" class="blocklist__link blocklist__link--with-icon-right">
                            <span class="blocklist__icon">
                                <i class="icon icon-right-open-big"></i>
                            </span>
                            <span class="blocklist__text">List</span>
                        </a>
                    </div>
                    <div class="blocklist__item">
                        <a href="#" class="blocklist__link blocklist__link--with-icon-right">
                            <span class="blocklist__icon">
                                <i class="icon icon-right-open-big"></i>
                            </span>
                            <span class="blocklist__text">List</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">Horizontal block list</div>
                </div>
                <div class="blocklist blocklist--horizon">
                    <div class="blocklist__item">
                        <a href="#" class="blocklist__link">
                            <span class="blocklist__text">List</span>
                        </a>
                    </div>
                    <div class="blocklist__item">
                        <a href="#" class="blocklist__link">
                            <span class="blocklist__text">List</span>
                        </a>
                    </div>
                    <div class="blocklist__item">
                        <a href="#" class="blocklist__link">
                            <span class="blocklist__text">List</span>
                        </a>
                    </div>
                    <div class="blocklist__item">
                        <a href="#" class="blocklist__link">
                            <span class="blocklist__text">List</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">Box container</div>
                </div>
                <div class="box">
                    <div class="box__body">
                        <div class="box__arrow"></div>
                        <div class="blocklist blocklist--insidebox">
                            <div class="blocklist__item">
                                <a href="#" class="blocklist__link blocklist__link--with-icon-left">
                                    <span class="blocklist__icon">
                                        <i class="icon icon-globe"></i>
                                    </span>
                                    <span class="blocklist__text">List</span>
                                </a>
                            </div>
                            <div class="blocklist__item">
                                <a href="#" class="blocklist__link blocklist__link--with-icon-left">
                                    <span class="blocklist__icon">
                                        <i class="icon icon-globe"></i>
                                    </span>
                                    <span class="blocklist__text">List</span>
                                </a>
                            </div>
                            <div class="blocklist__item">
                                <a href="#" class="blocklist__link blocklist__link--with-icon-right">
                                    <span class="blocklist__icon">
                                        <i class="icon icon-right-open-big"></i>
                                    </span>
                                    <span class="blocklist__text">List</span>
                                </a>
                            </div>
                            <div class="blocklist__item">
                                <a href="#" class="blocklist__link blocklist__link--with-icon-right">
                                    <span class="blocklist__icon">
                                        <i class="icon icon-right-open-big"></i>
                                    </span>
                                    <span class="blocklist__text">List</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">Badge</div>
                </div>
                <div class="badge">
                    <a href="#" class="badge__link">
                        <span class="badge__text">07.00</span>
                    </a>
                </div>
                <div class="badge">
                    <a href="#" class="badge__link is-disable">
                        <span class="badge__text">07.00</span>
                    </a>
                </div>
            </div>
            <div class="big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">Form</div>
                </div>
                <form action="#" class="input">
                    <div class="input__item input__item--with-icon">
                        <input type="email" class="input__field" placeholder="Email">
                        <div class="input__icon">
                            <i class="icon icon-email-envelope"></i>
                        </div>
                    </div>
                    <div class="input__item input__item--with-icon">
                        <input type="password" class="input__field" placeholder="Password">
                        <div class="input__icon">
                            <i class="icon icon-lock"></i>
                        </div>
                    </div>
                    <div class="input__item input__item--with-icon">
                        <input type="password" class="input__field" placeholder="Repeat Password">
                        <div class="input__icon">
                            <i class="icon icon-lock"></i>
                        </div>
                    </div>
                    <div class="input__item input__item--with-icon">
                        <select name="#" id="#" class="input__field">
                            <option value="" selected="selected">Pilih ruangan</option>
                            <option value="Value 1">Value 1</option>
                            <option value="Value 1">Value 1</option>
                            <option value="Value 2">Value 2</option>
                            <option value="Value 3">Value 3</option>
                        </select>
                        <div class="input__icon">
                            <i class="icon icon-chevron-down-2"></i>
                        </div>
                    </div>
                    <div class="input__item">
                        <textarea name="" id="" cols="30" rows="10" class="input__field"></textarea>
                    </div>

                </form>
            </div>
            <div class="big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">Panel</div>
                </div>
                <div class="panel">
                    <div class="panel__body">
                        <div class="panel__left">
                            <div class="thumb">
                                <div class="thumb__body thumb__body--card-list">
                                    <div class="thumb__img" style="background-image: url(http://sanyrosahotel.com/img/meeting.png);"></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel__middle">
                            <div class="heading">
                                <div class="heading__text heading__text--medium small-margin-trailer border-bottom">
                                    Blue Room
                                    <span class="heading__sub">Ampera 22</span>
                                </div>
                            </div>
                            <div class="yja">
                                <div class="yja-3">
                                    <div class="badge">
                                        <a href="#" class="badge__link">
                                            <span class="badge__text">16.00 - 17.00</span>
                                        </a>
                                    </div>
                                    <div class="badge">
                                        <a href="#" class="badge__link">
                                            <span class="badge__text">15.00 - 16.00</span>
                                        </a>
                                    </div>
                                    <div class="badge">
                                        <a href="#" class="badge__link is-disable">
                                            <span class="badge__text">11.00 - 13.00</span>
                                        </a>
                                    </div>
                                    <a href="#" class="font-small text-uppercase modal-trigger" data-modal="modal-time">View More</a>
                                </div>
                                <div class="yja-9">
                                    <div class="article">
                                        <h3>Fasilitas:</h3>
                                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consequatur rerum accusamus in neque ullam omnis iure at rem sit impedit laudantium cumque debitis aperiam perspiciatis, odit quas delectus eaque distinctio.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel__right">
                            <div class="panel__button">
                                <div class="button">
                                    <div class="button__body button__body--strong">Book Now</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">Table</div>
                </div>
                <table class="table">
                    <tr>
                        <th>Table Title</th>
                        <th>Table Title</th>
                        <th>Table Title</th>
                    </tr>
                    <tr>
                        <td>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </td>
                        <td>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </td>
                        <td>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </td>
                    </tr>
                </table>
            </div>
            <div class="big-margin-trailer">
                <div class="heading">
                    <div class="heading__text heading__text--big  margin-trailer border-bottom">Thumbnail</div>
                </div>
                <a href="#" class="thumb">
                    <div class="thumb__body">
                        <div class="thumb__img" style="background-image: url(http://www.download3dhouse.com/wp-content/uploads/2013/01/Fantasy-conference-room-design.jpg);"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<?php include_once("partials/main_footer.php"); ?>
<?php include_once("partials/meta_footer.php"); ?>
